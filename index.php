<?php
/*
Plugin Name:  _themename _pluginname
Plugin URI:
Description:  Adding Metaboxes for _themename
Version:      1.0.0
Author:       Isiah Obera
Author URI:   www.isiahobera.com
License:      GPL2
License URI:  https://www.gnu.org/license/gpl-2.0.html
Text-Domain:  _themename-_pluginname
Domain Path:  /languages
*/

if( !defined('WPINC')) {
  die;
}

include_once('includes/metaboxes.php');
include_once('includes/enqueue-assets.php');
